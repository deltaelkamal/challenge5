const express = require('express')
const app = express()
const port = 3000
const bodyParser= require('body-parser')
const jsonParser = bodyParser.json()



//routing challenge3
var http = require('http');
var fs = require('fs');
http.createServer(function (req, res) {
  fs.readFile('challenge3.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    return res.end();
  });
})



//routing challenge4
app.post('/challenge4', jsonParser, (req, res) => {
    let body = req.body;
    let result = ''
    let computerChoices = ['rock', 'paper', 'scissors'];

    function getComputerChoice() {
        const comp = Math.random();
        if( comp < 0.34 ) return 'rock';
        if( comp >= 0.34 && comp < 0.67 ) return 'paper';
        return 'scissors';
    }

    comp = computerChoices[getComputerChoice]
    
    function getResult(_input){
        if( player == comp ) return 'DRAW!';
        if( player == 'rock' ) return ( comp == 'paper' ) ? 'LOSE' : 'WIN';
        if( player == 'paper' ) return ( comp == 'scissors' ) ? 'LOSE' : 'WIN';
        if( player == 'scissors' ) return ( comp == 'rock' ) ? 'LOSE' : 'WIN';
    }

    
    // const choice = document.querySelectorAll('li img');
    // choice.forEach(function(cho) {
    //     cho.addEventListener('click', function() {
    //         const computerChoice = getComputerChoice();
    //         const playerChoice = cho.className;
    //         const result = getResult(computerChoice, playerChoice);
            
    //         const imgComputer = document.querySelector('.img-computer');
    //         imgComputer.setAttribute('src', 'assets/' + computerChoice + '.png');
            
    //         const info = document.querySelector('.info');
    //         info.innerHTML = result;
    //     });
    // });

    res.json({
        "comp_choice": comp,
        "winner": result
    })

})



//membuat variabel user
let user = [];

const verifyAuthToken = (req, res, next) => {
  let auth = req.headers['authorization'];
  let obj = user.find(o => o.token === auth)
  if (obj) {
    res.user = obj;
    next();
  } else {
    res.status(401).json({
      "message": "Unauthorized"
    });
  }
}

//routing register
app.post('/register', jsonParser, (req, res) => {
    let body = req.body;
    let email = body.email;
    let password = body.password;
    let r = (Math.random() + 1).toString(36).substring(7);

    user.push({ email, password, token: r });
    console.log(user);

    res.send({
        message: "Register berhasil"
    });
});

//routing login
app.post('/login', jsonParser, (req, res) => {
    let body = req.body;
    let email = body.email;
    let password = body.password;
    
    let obj = user.find(o => o.email === email)

    if (obj && obj.password === password){
        res.send({
            message : "Login Berhasil",
            token : obj.token
        });
    } else {
        res.send({
            message : "Login Berhasil"
        });
    }
});


app.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
})
